# GRaNIEverse 0.3.6 (2024-09-18)
- added two new arguments to `runGRaNIE()` that relate to the (experimental) GC correction features that GRaNIE offers to enable fine-tuning GC correction.
- adjusted the default values for some arguments to match the recommended single-cell specific adjustments as described in the GRaNIE single-cell vignette
- better documentation of arguments for `runGRaNIE`


# GRaNIEverse 0.3.5 (2024-09-17)
- small bugfix with the parameter `WNN_knn` from `prepareSeuratData_GRaNIE()`, non default values are now properly propagated


# GRaNIEverse 0.3.4 (2024-09-16)
- small bugfix with the parameter `recalculateVariableFeatures` from `prepareSeuratData_GRaNIE()`, now setting to `all` works (again)

# GRaNIEverse 0.3.3 (2024-09-06)
- a few updates and fixes to `prepareSeuratData_GRaNIE()`. It is now (again) possible to cluster by an existing metavariable.
- new default value for `minCellsPerCluster` in `prepareSeuratData_GRaNIE()`: 100 instead of 25. Note that this changes results for previous workflows when this parameter is not explicitly specified.
- new argument `cellTypeAnnotationCol` for `prepareSeuratData_GRaNIE()`
- documentation updates


# GRaNIEverse 0.3.2 (2024-08-29)
- fixed the error `'RNA' not found in this Seurat object` that was introduced in a 0.3.0 when the RNA assay was not called RNA. 

# GRaNIEverse 0.3.1 (2024-08-20)
- added two libraries to the DESCRIPTION file to make sure they are available to speed up data normalization
- added a fix for an error message related to the futures package that appears with bigger datasets

# GRaNIEverse 0.3.0 (2024-08-05)

- the function `prepareSeuratData_GRaNIE()` became a significant update, with clearer (and more) arguments. It is now possible to provide pre-normalized RNA data. In addition, additional parameters related to the LSI normalization for ATAC as well as parameters related to dimensionality reduction can now be specified.

# GRaNIEverse 0.2.0 (2024-06-24)

- made repository public
- small bugfixes 
- added website

# GRaNIEverse 0.1.2 (2023-05-08)

- added the first function to compare eGRNS: `compareConnections_upsetPlots()`. This function takes a list of GRaNIE eGRNs and produces a summary upset plot to compare among all input eGRNs. It can be used for TF-peak, TF-gene, TF-peak-gene and peak-gene pairs.
- small bug fixes


# GRaNIEverse 0.1.1 (2023-04-25)

- added `corMethod` to the GRaNIE batch function and the wrapper to allow overriding and specifying the correlation method
- fixed a bug in `prepareSeuratData_GRaNIE()` that appeared sometimes when using a custom pseudobulk variable as source for clustering. Thanks to Gerard for noticing and fixing!

# GRaNIEverse 0.1 (2023-04-17)

- first package version