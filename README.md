## GRaNIEverse: Extending GRaNIE


### Repository structure

This repository is for our *GRaNIEverse* package. This package and the accompanying [website](https://grp-zaugg.embl-community.io/GRaNIEverse) is **not** part of Bioconductor and independently build and maintained [in this repository](https://git.embl.de/grp-zaugg/GRaNIEverse). 

For the corresponding *GRaNIE* package, see [here](https://grp-zaugg.embl-community.io/GRaNIE). For the *GRaNPA* package, see [here](https://grp-zaugg.embl-community.io/GRaNPA).

### Installation

The *GRaNIEverse* package can be installed with the following one-liner, and you only need the ``devtools`` package for it:

`devtools::install_gitlab("grp-zaugg/GRaNIEverse", host = "git.embl.de", dependencies = TRUE)`

If you get the error that `GRaNIE` cannot be installed, try installing it separately beforehand, [see here for details](https://grp-zaugg.embl-community.io/GRaNIE).



We recommend installing all dependency packages so that you can use every functionality of the package, although the `dependencies = TRUE` part is not strictly necessary to install the package. If you run into any problems, let us know!


### Documentation

A very detailed documentation for the latest development version of the package is (soon) available here at [https://grp-zaugg.embl-community.io/GRaNIEverse](https://grp-zaugg.embl-community.io/GRaNIEverse). 


### Citation
**If you use our packages, please use the following citation:**

Kamal, A., Arnold, C., Claringbould, A., Moussa, R., Servaas, N.H., Kholmatov, M., Daga, N., Nogina, D., Mueller‐Dott, S., Reyes‐Palomares, A. and Palla, G., 2023. GRaNIE and GRaNPA: inference and evaluation of enhancer‐mediated gene regulatory networks. Molecular Systems Biology, p.e11627.; doi: [https://doi.org/10.15252/msb.202311627](https://doi.org/10.15252/msb.202311627)

### Bug Reports, Feature Requests and Contact Information

For issues, bugs, and feature request, please see the [Issue Tracker](https://git.embl.de/grp-zaugg/GRaNIEverse/issues). 

**We are actively working on the package and regularly improve upon features, add features, or change features for increased clarity. This sometimes results in minor changes to the workflow, changed argument names or other small incompatibilities that may result in errors when running a version of the package that differs from the version this vignette has been run for.**

If you have other questions or comments, feel free to contact us. We will be happy to answer any questions related to this project as well as questions related to the software implementation. For method-related questions, contact Judith B. Zaugg (judith.zaugg@embl.de). For technical questions, contact Christian Arnold (christian.arnold@embl.de). We will aim to respond in a timely manner.

 


