#' @param forceRerun A logical value indicating whether to force rerun the function and re-generate the output 
#' even if the output files already exist on disk or in the object. Default is FALSE.
